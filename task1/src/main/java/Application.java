import model.Kotik;
public class Application {
    public static void main(String[] args) {
        Kotik kot1 = new Kotik();
        kot1.setKotik("Vasia", 5, 5, "Miu Miu");
        Kotik kot2 = new Kotik("Barsik", 6, 6, "Meow Meow");
        System.out.println("Есть кот с кличкой " + kot2.getName() + " весом " + kot2.getWeight() + " кило.");
        if (kot1.getMeow().equals(kot2.getMeow())) {
            System.out.println("Коты мяукают одинаково.");
        } else {
            System.out.println("Коты мяукают по разному.");
        }
        kot2.liveAnotherDay();
        System.out.println("Количество котов: " + Kotik.getCount());
    }
}